# from https://stackoverflow.com/questions/14132789/relative-imports-for-the-billionth-time
if __package__ is None or __package__ == '':
    # uses current directory visibility
    import concomitant_at
else:
    # uses current package visibility
    import timeflow.concomitant_at as concomitant_at

def buildOrderedTimestampAndLabelList(results):
    # Calculate at the relevant time stamp
    incremental_dictionary_timestampXnodeLabelArray = {}
    def __internal_calculation_of_simultaneity(value):
        processuses_list = concomitant_at.findProcessusesAtTime(results, value)
        return processuses_list
    for result in results:
        for interval in result.time_intervals:
            if not incremental_dictionary_timestampXnodeLabelArray.get(interval['start']):
                incremental_dictionary_timestampXnodeLabelArray[interval['start']] = __internal_calculation_of_simultaneity(interval['start'])
    tspXnodeLabelArray_as_list = []
    for key in incremental_dictionary_timestampXnodeLabelArray:
        value = incremental_dictionary_timestampXnodeLabelArray[key]
        tspXnodeLabelArray_as_list.append({'label_list': value, 'timestamp': key})
    tspXnodeLabelArray_as_list.sort(key = lambda v: v['timestamp'])
    return tspXnodeLabelArray_as_list

def firstConcomitantInstantFromOrderedTimestampXLabelList(ordered_timstampXlabelList, *node_labels):
    ''' Return the first instant in which all the node_label(s) are concomitant. If not found in the result, return None'''
    node_labels_as_set = set(node_labels)
    for instantXlabel_list in ordered_timstampXlabelList:
        labelList_as_set = set(instantXlabel_list['label_list'])
        if node_labels_as_set.issubset(labelList_as_set):
            return {'starting_instant': instantXlabel_list['timestamp'], 'concomitant_events': labelList_as_set}
    return None



