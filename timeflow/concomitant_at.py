import argparse
import py2neo
import yaml
# from https://stackoverflow.com/questions/14132789/relative-imports-for-the-billionth-time
if __package__ is None or __package__ == '':
    # uses current directory visibility
    import build_sequences
else:
    # uses current package visibility
    import timeflow.build_sequences as build_sequences

def findProcessusesAtTime(results, time):
    out = []
    for result in results:
        index = dichotomialSearch(result.time_intervals, time)
        out.append(result.path[index])
    return out

def dichotomialSearch(time_intervals, time):
    inf_index = 0
    sup_index = len(time_intervals) - 1
    while True:
        index = inf_index + int((sup_index - inf_index) / 2)
        current_interval = time_intervals[index]
        if current_interval['start'] <= time and current_interval['end'] >= time:
            return index
        elif current_interval['start'] > time:
            sup_index -= max(int((sup_index - inf_index) / 2), 1)
        elif current_interval['end'] < time:
            inf_index += max(int((sup_index - inf_index) / 2), 1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = """ 
""", formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('config', help="""configuration of the walker. Fields are :    
    'node_label' : the label cypher declaration of nodes
    'relationship_type' : the relationship type declaration,
    'identifier_attribute' : node attribute to identify nodes,
    'flow_attribute' : node attribute carrying the time flow struture,
    'starting_identifier' : Initial node, relative to the identifier attribute,
    'starting_shift' : time flow at start, default 0
    'url': url with credentials of the bolt endpoint. Same as -u option""", default = "\{'url': 'http://neo4j:pic3.14@localhost:7474/db/data/'\}")
    parser.add_argument('-u', dest = 'url', help="""url with credentials of the bolt endpoint""", nargs= "?")
    parser.add_argument('-t', dest = 'time', help="""time to reach""", default=300)
    args = parser.parse_args()
    config = yaml.load(args.config, Loader=yaml.FullLoader)
    url = None
    if 'url' in config: 
        url = config['url']
    if not args.url is None:
        url = str(args.url)
    if url is None:
        print('no url provided. Check your configuration or url (-u) option')
        quit()
    time_to_reach = int(args.time)
    task = build_sequences.TimeRunner(config, url)
    print(findProcessusesAtTime(task.nodesAt(time_to_reach), time_to_reach))