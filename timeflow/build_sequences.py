# -*- coding: utf-8 -*-
""" Parcour a graph through cypher through a time alike dimension
"""
__author__ = "Guilhem Heinrich"
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "Guilhem Heinrich"
__email__ = "guilhem.heinrich@inra.fr"
__status__ = "Prototype"

import argparse
import py2neo
import ast
import yaml

class PathStructure:
    def __init__(self, pathStructure = None, config = {}):
        if pathStructure != None:
            self.__time_spent = pathStructure.time_spent
            self.path = list(pathStructure.path)
            self.__time_intervals = list(pathStructure.time_intervals)
            self.data =list(pathStructure.data)
        else:
            self.__time_intervals = []
            self.__time_spent = config['starting_shift']
            self.path = [config['starting_identifier']]
            self.data = []
    
    @property
    def time_spent(self):
        return self.__time_spent
    
    @time_spent.setter
    def time_spent(self, ts):
        if (ts >= self.__time_spent):
            self.time_intervals.append({'start' : self.__time_spent, 'end' : ts})
            self.__time_spent = ts
        else:
            raise ValueError('next time_spent value must be greater or equal to the current one !')
    
    @property
    def time_intervals(self):
        return self.__time_intervals

class TimeRunner:
    __default_config = {
        'node_label' : ':SimulationTemp',
        'relationship_type' : ['after', 'dependsOn'],
        'identifier_attribute' : 'uri',
        'flow_attribute' : 'last',
        'starting_identifier' : 'Processus_0',
        'starting_shift' : 0,
        'url': 'http://neo4j:neo4j@localhost:7474/db/data/'
    }
    def __init__(self, url = None, config = {}):
        if config.get('starting_shift') and config['starting_shift'] < 0:
            raise ValueError('Starting shift MUST BE greater or equal to 0')
        self.config = TimeRunner.__default_config.copy()
        self.config.update(config)
        if not url is None:
            self.config['url'] = url 
        self.secure_graph = py2neo.Graph(self.config['url'])
        self.initiator = PathStructure(config = self.config)
        self.__time_intervals = []
        first_node = self.secure_graph.run("MATCH (first" + self.config['node_label'] + " { " + self.config['identifier_attribute'] + ": '" + self.config['starting_identifier'] + "'}) RETURN first").data()
        if (len(first_node)) == 0:
            raise RuntimeError('The root node could not be found. Check the node_label, identifier_attribute and starting_identifier configuration value')
        if self.config['starting_shift'] > first_node[0]['first'][self.config['flow_attribute']]:
            raise RuntimeError('The root process has a duration lower than the starting shift, hence this error')
        self.initiator.time_spent += first_node[0]['first'][self.config['flow_attribute']]
        self.initiator.data.append(first_node[0]['first'])
    
    def _compute(self, pathStructure):
        nexts = self.secure_graph.run("MATCH (" + self.config['node_label'] + " { " + self.config['identifier_attribute'] + ": '" + pathStructure.path[-1] + "'}) -[relationship]-> (next " + self.config['node_label'] + ")" +
        "WHERE type(relationship) IN ['" + "', '".join(self.config['relationship_type']) + "' ]" +
        " RETURN next").data()
        out = list()
        for record in nexts:
            newPathStructure = PathStructure(pathStructure)
            newPathStructure.path.append(record['next'][self.config['identifier_attribute']])
            newPathStructure.time_spent += record['next'][self.config['flow_attribute']]
            newPathStructure.data.append(record['next'])
            out.append(newPathStructure)
        return out
    
    def nodesAt(self, time_to_reach = 150):
        results = list()
        def compute(pathStructureList):
            for pathStructure in pathStructureList:
                if pathStructure.time_spent < time_to_reach:
                    nexts = self._compute(pathStructure)
                    compute(nexts)
                else:
                    results.append(pathStructure)
        compute([self.initiator])
        return results

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = """ 
""", formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('config', help="""configuration of the walker. Fields are :    
    'node_label' : the label cypher declaration of nodes
    'relationship_type' : the relationship type declaration,
    'identifier_attribute' : node attribute to identify nodes,
    'flow_attribute' : node attribute carrying the time flow struture,
    'starting_identifier' : Initial node, relative to the identifier attribute,
    'starting_shift' : time flow at start, default 0
    'url': url with credentials of the bolt endpoint. Same as -u option""", default = "{'url': 'http://neo4j:pic3.14@localhost:7474/db/data/'}", nargs= "?")
    parser.add_argument('-u', dest = 'url', help="""url with credentials of the bolt endpoint""", nargs= "?")
    parser.add_argument('-t', dest = 'time', help="""time to reach""", default=300)
    args = parser.parse_args()
    config = yaml.load(args.config, Loader=yaml.FullLoader)
    url = None
    if 'url' in config: 
        url = config['url']
    if not args.url is None:
        url = str(args.url)
    if url is None:
        print('no url provided. Check your configuration or url (-u) option')
        quit()
    time_to_reach = int(args.time)
    task = TimeRunner(url = url, config = config)
    for res in task.nodesAt(time_to_reach):
        print(res.path)
        print(res.time_intervals)