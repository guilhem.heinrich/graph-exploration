from bokeh.io import show
from bokeh.models import LogColorMapper
from bokeh.palettes import Viridis6 as palette
from bokeh.plotting import figure

import json
import colorcet as cc


# color_attribute should be able to be a function applied to data and returning a value
def showTimeConcomitance(results:[], color_attribute):
    Y_index = 0
    delta_x = 0.1
    thickness_y = 0.8
    xs = []
    ys = []
    event_identifier = []
    color_identifier = []
    all_data = []
    for _index in range(len(results)):
        event_sequence = results[_index].path
        intervals_sequence = results[_index].time_intervals
        data = results[_index].data
        for interval_index in range(len(intervals_sequence)):
            interval = intervals_sequence[interval_index]
            xs.append([float(interval['start']) + delta_x, float(interval['end']) - delta_x, float(interval['end']) - delta_x, float(interval['start']) + delta_x ])
            ys.append([_index  - thickness_y / 2, _index - thickness_y / 2, _index + thickness_y / 2, _index + thickness_y / 2 ])
            event_identifier.append(event_sequence[interval_index])
            color_identifier.append(data[interval_index][color_attribute])
            all_data.append(json.dumps(data[interval_index]))
    # Convert to a reasonable int each color_identifier
    unique_identifier = list(set(color_identifier))
    color_identifier = [int((unique_identifier.index(color) / (len(unique_identifier) - 1)) * 255) for color in color_identifier]
    data=dict(
        x=xs,
        y=ys,
        label=event_identifier,
        color=color_identifier,
        data= all_data
    )
    color_mapper = LogColorMapper(palette=cc.CET_R1)
    TOOLS = "wheel_zoom,reset,hover,save, pan"
    p = figure(
        title="Timeline of events starting at " + results[0].path[0], tools=TOOLS,
        tooltips=[
            ("Data", "@data"), ("Time", "$x")
        ])
    p.ygrid.grid_line_color = None
    p.xgrid.grid_line_color = "#dddddd"
    p.hover.point_policy = "follow_mouse"
    p.patches('x', 'y', source=data,
            fill_color={'field': 'color', 'transform': color_mapper},
            fill_alpha=0.7, line_color="white", line_width=0.5)
    show(p)

